from datetime import date
from dateutil.relativedelta import relativedelta

print("Welcome to the simple Age Calculator. Let's start...")

date1, date2, x = None, None, "a"
while True:
    try:
        year, month, day = input(
            "Enter " + x + " date (YYYY-MM-DD): ").split("-")
        if date1 is None:
            date1 = date(int(year), int(month), int(day))
            x = "another"
        else:
            date2 = date(int(year), int(month), int(day))
            break
    except:
        print("Invalid date format. Try again...")

print("Start date: {} \nEnd date: {}.\n".format(date1, date2))

age = relativedelta(date1, date2)

print("You are now {} year {} month {} day old.".format(
    age.years, age.months, age.days))

print("Wish your long awesome life. Thank You.")

# Currently for a reason i have to count some age. I know that it's kill many time.
# On this time, i think why I'm not creating a simple age calculator? 😍😍😍
