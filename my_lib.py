import math


class MyLib:
    def binary_search(self, data, key):
        last = len(data)
        m = math.floor(last / 2)
        if key == data[m]:
            return m + 1
        elif key < data[m]:
            return self.binary_search(data[0:m-1], key)
        elif key > data[m]:
            return self.binary_search(data[m+1:last], key)
        else:
            return False
